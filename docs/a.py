def dlog(n):
    """
    n -- entier naturel non nul
    renvoie dlog(n)
    """
    k = 0
    p = 1 # contiendra les puissances de 10 successives
    while p <= n:
        k += 1
        p *= 10
    return k-1



def Dlog(n):
    """
    n -- entier naturel non nul
    renvoie dlog(n)
    """
    k = 0
    while 10**k <= n:
        k += 1
    return k-1

print(Dlog(100))
