# La fonction  dlog


!!! attention

    Cette page n'est pour le moment pas une priorité. 
    Travaillez là uniquement  si vous en avez le temps, c'est à dire si le reste
    est travaillé et su. Nous reviendrons dessus plus tard dans l'année
    lorsque nous en aurons besoin.





On définit une fonction  sur les entiers > 0.

!!! note
    Ceux qui suivront la spécialité mathématiques en terminale découvriront la fonction "plus 
    générale" logarithme.

 


!!! Définition
    
    Tout entier naturel $n$ (non nul) est compris entre deux puissances de 10:
    $10^k \leqslant n < 10^{k+1}$.
    
    Le nombre $k$ sera appelé dlog(n).
    
    
!!! remarque

    dlog(n) + 1 est le nombre de chiffres de l'écriture décimale de n.
    
    Par exemple: $100 \leqslant 122  < 1000$, donc dlog(122) = 2. Et le nombre de chiffres de 122 est 3.
    
    
##  Exercice  1

Donner les valeurs de dlog(99), dlog(100), dlog(1000), dlog(101), dlog(1).

??? solution

    + $10 \leqslant 99 < 10^2$ donc dlog(99) = 1.
    + $10^2 \leqslant 100 < 10^3$ donc dlog(100) =  2.
    + $10^2 \leqslant 101 < 10^3$ donc dlog(101) =  2.
    + $10^3 \leqslant 1000 < 10^4$ donc dlog(100) =  3.
    + $10^0 \leqslant 1 < 10^1$ donc dlog(1) =  0.
    
    
    
 
##  Exercice 2  
Proposer un corps possible pour la fonction python suivante:

```python
def dlog(n):
    """
    n -- entier naturel non nul
    renvoie dlog(n)
    """
```


??? solution "une solution"

    ```python
    def Dlog(n):
        """
        n -- entier naturel non nul
        renvoie dlog(n)
        """
        k = 0
        while 10**k <= n:
            k += 1
        return k-1
    ```
    
    
Proposer maintenant une solution qui n'utilise pas ** pour calculer les puissances de 10 (cet exercice des calculs
des puissances successives sans utiliser ** a déjà
été fait précédemment, vous devriez donc savoir le traiter).



??? solution "Un code possible"

    ```python
    def dlog(n):
        """
        n -- entier naturel non nul
        renvoie dlog(n)
        """
        k = 0
        p = 1 # contiendra les puissances de 10 successives
        while p <= n:
            k += 1
            p *= 10
        return k-1
    ```

 
