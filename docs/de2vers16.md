# Du binaire à l'hexadécimal


## 16 est une puissance de 2

Comme $16=2^4$, il est possible de passer d'une écriture binaire à une écriture hexadécimale
de la façon suivante:

1. On regroupe les bits de l'écriture binaire par paquets de quatre, en ajoutant au besoin à gauche des 0 pour que
le nombre de bits soit un multiple de 4. Par exemple $n = 101011_{2}$ sera réécrit $n = 0010 \, 1011_{2}$.
2. On traduit ensuite chaque paquet de 4 en base 16. Dans l'exemple précédent, 
le paquet $0010_{2}$ se traduit par $2_{16}$ et le paquet $1011_{2}$ se traduit par $b_{16}$.
3. On "concatène" ces écritures en base 16, cela donne l'écriture en base 16 de l'entier. Ainsi: $n = 2b_{16}$.


!!! note "Explication succincte"
    Sans rentrer dans des écritures trop formelles, on peut expliquer "pourquoi ça marche" avec cet exemple.
    
    On remarque déjà que sur 4 bits, on peut écrire en binaire les entiers de $0000_{2} = 0$ à $1111_{2} = 15$, ce qui
    correspond aux chiffres de la base 16.
    
    Et $n = 0010 \, 1011_{2}$ se réécrit: 
    $$
    n = \left( 0\times 2^7 + 0 \times 2^6 + 1\times 2^5 + 0\times 2^4 \right) + \left( 0\times 2^3 + 0 \times 2^2 + 1\times 2^1 + 0\times 2^0 \right)
    $$
    ou encore:
    $$
    n = \left( 0\times 2^3 + 0 \times 2^2 + 1\times 2^1 + 0\times 2^0 \right) \times 2^4 + \left( 0\times 2^3 + 0 \times 2^2 + 1\times 2^1 + 0\times 2^0 \right)
    $$
    c'est à dire:
    $$
    n = \left( 0\times 2^3 + 0 \times 2^2 + 1\times 2^1 + 0\times 2^0 \right) \times 16^1 + \left( 0\times 2^3 + 0 \times 2^2 + 1\times 2^1 + 0\times 2^0 \right) \times 16^0
    $$
    
    On se convaincra facilement que la démarche est générique. On aboutit ainsi bien à une écriture $\sum_{i} c_i\times 16^i$
    où les coefficients $c_i$ sont compris entre 0 et 15.
 
 
## Correspondance



| binaire | hexadécimal |
|:-------:|:-----------:|
|   0000  |      0      |
|   0001  |      1      |
|   0010  |      2      |
|   0011  |      3      |
|   0100  |      4      |
|   0101  |      5      |
|   0110  |      6      |
|   0111  |      7      |
|   1000  |      8      |
|   1001  |      9      |
|   1010  |      a      |
|   1011  |      b      |
|   1100  |      c      |
|   1101  |      d      |
|   1110  |      e      |
|   1111  |      f      |

 
 
 


## Exercice 1.

Donner l'écriture hexadécimale des entiers ci-dessous en suivant la démarche énoncée ci-dessus 
(regroupement des bits par groupe de 4, etc..):

1. $n = 11001_{2}$
2. $m = 101010_{2}$
3. $p = 1000010_{2}$
4. $d = 11_{2}$
5. $t = 111111111111_{2}$

??? solution "Solution"
    1. $n = 0001\, 1001_{2} = 19_{16}$
    2. $m = 0010\, 1010_{2} = 2a_{16}$
    3. $p = 0100\, 0010_{2} = 42_{16}$
    4. $d = 0011_{2} = 3_{16}$
    5. $t = 1111\, 1111\, 1111_{2} = fff_{16}$

    
    
    
    
    
    
    
    
 
