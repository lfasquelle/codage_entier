# &Eacute;critures d'un entier positif

 
 
Nous écrivons usuellement les entiers en base 10. Mais ils peuvent s'écrire en base b quelconque où b est 
un entier naturel au moins égal à deux.

Les bases fréquemment utilisées en informatique sont les bases 2 et 16.

!!! attention

    Plusieurs exercices, comme dans d'autres cours qui suivront, sont marqués "facultatif".   
    Cela signifie que vous ne les travaillerez qu'après avoir travaillé et compris l'intégralité du reste
    des pages. Ces exercices facultatifs seront souvent des exercices vous permettant de vous entraîner 
    à la programmation (ils n'ont pas à être connus parfaitement mais les travailler peut vous aider
    à mieux comprendre certains points).
