# Décaler les bits



    
## Exercice 1


On dispose de l'écriture binaire d'un entier d. On ajoute un 0 à la fin (à droite) de cette écriture pour obtenir
un entier f.  
Quelle opération permet de passer de l'entier d à l'entier f?

Exemples:

+ d = 11<sub>deux</sub> donne f = 110<sub>deux</sub>.
+ d = 1010<sub>deux</sub> donne f = 10100<sub>deux</sub>.


??? solution

    Cette opération revient à multiplier l'entier par 2:  f = 2d.  
    
    Pour bien comprendre cela, écrivons le détail sur les exemples donnés.
    
    + 11<sub>deux</sub> =  1 &times; 2<sup>1</sup> + 1 &times;  2<sup>0</sup>.  
    On ajoute un 0 en fin d'écriture:  
    110<sub>deux</sub> =  1 &times; 2<sup>2</sup> + 1 &times; 2<sup>1</sup> + 0  &times; 2<sup>0</sup>      
    soit 110<sub>deux</sub> = 2 &times;  (1 &times; 2<sup>1</sup> + 1 &times; 2<sup>0</sup>),   
    c'est à dire:
    110<sub>deux</sub> = 2 &times; 11<sub>deux</sub>.
    
    
    + 1010<sub>deux</sub> = 1 &times; 2<sup>3</sup> + 0 &times; 2<sup>2</sup> + 1 &times; 2<sup>1</sup> + 0   &times; 2<sup>0</sup>  
    et 10100<sub>deux</sub> = 1 &times; 2<sup>4</sup> + 0 &times; 2<sup>3</sup> + 1 &times; 2<sup>2</sup> + 0   &times; 2<sup>1</sup>   + 0   &times; 2<sup>0</sup>  
    soit 10100<sub>deux</sub> = 2 &times; (1 &times; 2<sup>3</sup> + 0 &times; 2<sup>2</sup> + 1 &times; 2<sup>1</sup> + 0   &times; 2<sup>0</sup>)   
    c'est à dire 10100<sub>deux</sub> = 2 &times; 1010<sub>deux</sub>.
     
 

     
## Exercice 2

On dispose de l'écriture binaire d'un entier d. On supprime le chiffre le plus à droite de cette écriture.  
Quel lien entre l'entier d de départ et l'entier f obtenu ?

Exemples:

+ Pour d = 110<sub>deux</sub>, on a f = 11<sub>deux</sub>.
+ Pour d = 1101<sub>deux</sub>, on a f = 110<sub>deux</sub>.


??? solution

    f est le quotient de la division euclidienne de d par 2.  On a donc f = d//2.
    
    Pour bien comprendre cela, détaillons sur les exemples:
    
    + d = 110<sub>deux</sub>, soit d = 1 &times;  2<sup>2</sup> + 1 &times;  2<sup>1</sup> +  0 &times;  2<sup>0</sup>,
    soit d = 2 &times; ( 1 &times;  2<sup>1</sup> + 1 &times;  2<sup>0</sup>) + 0.
    On a écrit d sous la forme 2q+0 où q est entier:  le 0 final est le reste de la division de d par 2
    et q est le quotient.  
    Barrer le 0 final revient à remplacer d par q  (on a q = f).
    
    +  d = 1101<sub>deux</sub>, 
    soit d = 1 &times;  2<sup>3</sup> + 1 &times;  2<sup>2</sup> +  0 &times;  2<sup>1</sup> + 1 &times;  2<sup>0</sup>.   
    Soit d = 2 &times; (1 &times;  2<sup>2</sup> + 1 &times;  2<sup>1</sup> +  0 &times;  2<sup>0</sup>) + 1.   
    Le 1 final est le reste de la division de d par 2 et le nombre f est le quotient. 
    
    
 
