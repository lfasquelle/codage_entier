# De l'hexadécimal au décimal


## Les chiffres en écriture hexadécimale

Pour écrire un entier en hexadécimal (c'est à dire en base seize), on a besoin de seize chiffres. On utilisera a, b, c, d, e, f
avec la correspondance suivante:   

|  a |  b |  c |  d |  e |  f |
|:--:|:--:|:--:|:--:|:--:|:--:|
| 10 | 11 | 12 | 13 | 14 | 15 |


Exemples:

+ $f1a_{\text{seize}} = 15\times 16^2 + 1 \times 16^1 + 10 \times 16^0$,  
on a donc $f1a_{\text{seize}} = 3866$.
+ $20b3_{\text{seize}} = 2\times 16^3 + 0\times 16^2 + 11 \times 16^1 + 3 \times 16^0$, 
soit  $20b3_{\text{seize}} = 8371$.


## Exercice 1.

1. Donner l'écriture décimale de l'entier $n = 2a_{\text{seize}}$.
2.  Donner l'écriture décimale de l'entier $m = dada_{\text{seize}}$.

??? solution "Solution"
    1. $n = 2a_{\text{seize}} = 2\times 16 +10= 42$.
    2. $m = dada_{\text{seize}} = 13\times 16^3 + 10\times 16^2 + 13 \times 16^1 + 10 \times 16^0 = 56026$.
    
    
 
## Exercice 2.

Quels sont les entiers qui s'écrivent, en hexadécimal, sous la forme d'un 1 suivi uniquement de 0 
(1<sub>seize</sub>, 10<sub>seize</sub>, 100<sub>seize</sub>, 1000<sub>seize</sub>, 10000<sub>seize</sub>...)?


??? solution "Résolution"
    
    + $1_{\text{seize}} = 1 = 16^0$
    + $10_{\text{seize}} = 16^1$
    + $100_{\text{seize}} =  16^2$
    + $1000_{\text{seize}} =  16^3$
    + ...
    + $100...0_{\text{seize}} =  16^m$ où $m$ est le nombre  de zéros.
    
    Les nombres 10...0 sont les puissances de 16.  


