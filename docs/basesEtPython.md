# Fonctions prédéfinies en langage Python



!!! important

    **La lecture de cette page est facultative**.

    Nous avons vu en exercice des exemples de fonctions python permettant le passage d'une base à l'autre. 
    Il existe également des fonctions prédéfinies dans le langage Python.

    Vous n'avez pas à connaître par coeur ces fonctions.
    
    
## Du décimal vers le binaire

La fonction python `bin` renvoie l'écriture binaire de l'entier donné en argument.

``` python
for k in range(20):
    print(f"L'écriture binaire de {k} est {bin(k)}.")
```

donne:

```
L'écriture binaire de 0 est 0b0.
L'écriture binaire de 1 est 0b1.
L'écriture binaire de 2 est 0b10.
L'écriture binaire de 3 est 0b11.
L'écriture binaire de 4 est 0b100.
L'écriture binaire de 5 est 0b101.
L'écriture binaire de 6 est 0b110.
L'écriture binaire de 7 est 0b111.
L'écriture binaire de 8 est 0b1000.
L'écriture binaire de 9 est 0b1001.
L'écriture binaire de 10 est 0b1010.
L'écriture binaire de 11 est 0b1011.
L'écriture binaire de 12 est 0b1100.
L'écriture binaire de 13 est 0b1101.
L'écriture binaire de 14 est 0b1110.
L'écriture binaire de 15 est 0b1111.
L'écriture binaire de 16 est 0b10000.
L'écriture binaire de 17 est 0b10001.
L'écriture binaire de 18 est 0b10010.
L'écriture binaire de 19 est 0b10011.
```


## Du décimal vers l'hexadécimal


La fonction python `hex` renvoie l'écriture hexadécimale de l'entier donné en argument.

``` python
for k in range(20):
    print(f"L'écriture hexadécimale de {k} est {hex(k)}.")
```

donne:

```
L'écriture hexadécimale de 0 est 0x0.
L'écriture hexadécimale de 1 est 0x1.
L'écriture hexadécimale de 2 est 0x2.
L'écriture hexadécimale de 3 est 0x3.
L'écriture hexadécimale de 4 est 0x4.
L'écriture hexadécimale de 5 est 0x5.
L'écriture hexadécimale de 6 est 0x6.
L'écriture hexadécimale de 7 est 0x7.
L'écriture hexadécimale de 8 est 0x8.
L'écriture hexadécimale de 9 est 0x9.
L'écriture hexadécimale de 10 est 0xa.
L'écriture hexadécimale de 11 est 0xb.
L'écriture hexadécimale de 12 est 0xc.
L'écriture hexadécimale de 13 est 0xd.
L'écriture hexadécimale de 14 est 0xe.
L'écriture hexadécimale de 15 est 0xf.
L'écriture hexadécimale de 16 est 0x10.
L'écriture hexadécimale de 17 est 0x11.
L'écriture hexadécimale de 18 est 0x12.
L'écriture hexadécimale de 19 est 0x13.
```


## Du binaire vers le décimal

Dans un terminal:

``` python
>>> 0b1101
13
```

 

``` python
>>> int('0b1101',2)
13
>>> int('1101',2)
13
```

 

## De l'hexadécimal vers le décimal


Dans un terminal:


``` python
>>> 0x1a
26
```

 

``` python
>>> int('0xa',16)
10
>>> int('a',16)
10
```
 
