# De l'hexadécimal au binaire

## Le procédé

On utilise le processus "inverse" de celui qui nous a permis de passer du binaire
à l'hexadécimal: chacun des chiffres hexadécimaux est remplacé par sa traduction en binaire sur 4 bits.

Exemples:

1. $a2_{seize} = 1010\, 0010_{deux}$
2. $3b_{seize} = 0011\, 1011_{deux} = 11\, 1011_{deux}$


## Exercice.

&Eacute;crire en binaire les nombres suivants (en utilisant la technique ci-dessus):

1. $n = 4f_{seize}$
2. $m = d51_{seize}$
3. $p = efface_{seize}$
4. $q = feedbac_{seize}$


??? solution "Solution"
    1. $n = 0100\, 1111_{deux} =  100\, 1111_{deux}$.
    2. $m = 1101\, 0101\, 0001_{deux}$
    3. $p = 1110\,  1111\,  1111\,  1010\,  1100\,  1110_{deux}$
    4. $q = 1111\, 1110\, 1110\, 1101\, 1011\, 1010\, 1100_{deux}$
